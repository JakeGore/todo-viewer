const glob = require('glob')

// Get the text from the specified files
module.exports = (path) => {
  return new Promise((resolve, reject) => {
    glob(path, (err, files) => {
      if (err) reject(new Error('An error occurred'))
      if (files.length === 0) return console.log('No files matched the pattern')
      files.forEach((file, key) => {
        if (file.substring(0, 2) === './') return
        files[key] = './' + file
      })
      resolve(files)
    })
  })
}
